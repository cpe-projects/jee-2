# README
Mathias Arlaud - Laurent Morel

# Projet

- git : https://gitlab.com/cpe-projects/jee-2
- screencast : https://youtu.be/y0vXgZS9zVc

- Parties réalisées : toutes
- Parties non traitées : aucune

- Parties supplémentaires : développement du watcher, CRUD sur les content côté nodejs

# NodeJS et React

## Installation
Afin d'installer les dépendences node, exécutez la commande `make install`
Si vous êtes sous Windows, il faut ensuite copier les dossier `build` des applications `react` dans les dossier `nodejs/public` (liens symboliques)

## Configuration
Pour activer le bouchon d'authentification, modifiez le fichier `nodejs/config.json` et passer le boolen `fakeLogin` a `true`.

## Lancement
Depuis la racine du projet, exécutez la commande `make run`

## Utilisation
Interface d'administration : http://localhost:1337/admin/
Watcher : http://localhost:1337/watch/

# JavaEE
La table _user_ est recréée à chaque lancement de l'application. 
Deux utilisateurs de tests sont présents dans `dump.sql`, les mots de passes sont en clair.
