package fr.cpe.jms;

import fr.cpe.models.UserModel;

public interface IMessageReceiver {

    UserModel receiveMessage();

}
