package fr.cpe.models;

import javax.json.Json;
import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "user")
@NamedQuery(name = "UserModel.byLoginAndPassword", query = "select u from UserModel u where u.login=:login and u.password=:password")
public class UserModel implements Serializable {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private String login;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false)
    private String firstname;

    @Column(nullable = false)
    private String lastname;

    @Column(nullable = false)
    // @Enumerated(EnumType.STRING)
    private String role;

    public UserModel() {
    }

    public String toJson(boolean isAuth) {
        return Json.createObjectBuilder()
                .add("login", login)
                //.add("password", password)
                .add("validAuth", isAuth)
                .add("firstname", firstname)
                .add("lastname", lastname)
                .add("role", role)
                .build()
                .toString();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
