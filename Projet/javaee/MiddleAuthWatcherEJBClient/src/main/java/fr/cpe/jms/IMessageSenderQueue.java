package fr.cpe.jms;

import fr.cpe.models.UserModel;

public interface IMessageSenderQueue {

    void sendMessage(String message);
    void sendMessage(UserModel user);

}
