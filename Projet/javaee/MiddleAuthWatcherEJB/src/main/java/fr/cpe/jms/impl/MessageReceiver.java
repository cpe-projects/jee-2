package fr.cpe.jms.impl;

import fr.cpe.models.UserModel;
import fr.cpe.jms.IMessageReceiver;
import org.hibernate.validator.internal.util.logging.LoggerFactory;

import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.Queue;
import java.util.logging.Logger;

@Stateless
//@LocalBean
public class MessageReceiver implements IMessageReceiver {
    private final static Logger logger = Logger.getLogger(MessageReceiver.class.getCanonicalName());

    @Inject
    JMSContext context;

    @Resource(mappedName = "java:/jms/queue/watcherqueue")
    Queue queue;

    public UserModel receiveMessage() {
        JMSConsumer consumer = context.createConsumer(queue);

        logger.info("Message reçu sur la queue watcherqueue");
        return consumer.receiveBody(UserModel.class, 1000);
    }
}
