package fr.cpe.jms.impl;


import fr.cpe.models.UserModel;
import fr.cpe.jms.IMessageSenderTopic;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.Topic;
import java.util.logging.Logger;

@Stateless
public class MessageSender implements IMessageSenderTopic {
    private final static Logger logger = Logger.getLogger(MessageSender.class.getCanonicalName());

    @Inject
    JMSContext context;

    @Resource(mappedName = "java:/jms/watcherAuthJMSx")
    Topic topic;

    @Override
    public void sendMessage(String message) {
        logger.info("Envoi d'un message texte sur le topic watcherAuthJMSx");
        context.createProducer().send(topic, message);
    }

    @Override
    public void sendMessage(UserModel user) {
        logger.info("Envoi d'un message objet UserModel sur le topic watcherAuthJMSx");
        context.createProducer().send(topic, user);
    }

}
