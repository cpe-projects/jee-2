'use strict';

const request = require('request-promise-native');
const CONFIG = JSON.parse(process.env.CONFIG);

class AuthController
{

    static login(req, res)
    {
        AuthController._loginUser(req.body.login, req.body.password).then((user) => {
            res.json(user)
               .end();
        }).catch((err) => {
            res.status(403)
               .send('Invalid credentials')
               .end(); 
        });
    }

    static _loginUser(username, password) {

        if(CONFIG.fakeLogin) {
            const fakeUser = {
                firstname: "Laurent",
                lastname: "MOREL",
                login: "laurent",
                role: "admin",
                validAuth: true
            }

            return Promise.resolve(fakeUser);
        }

        return request({ 
            url: '/auth/login',
            baseUrl: CONFIG.userApi,
            method: 'POST',
            json: {
                login: username,
                password: password
            }
        });

    }

}

module.exports = AuthController;
