'use strict';

const contentModel = require('../models/content.model');
const generateUUID = require('../utils/utils').generateUUID;
const mime = require('mime');
const fs = require('fs');
const _p = require('util').promisify;

class ContentController
{
    constructor () {}

    static list (request, response)
    {
        contentModel.list((err, contents) => {
            if(err) return ContentController._error(response, 500, err);
            response.json(contents);
        })
    }

    static create (request, response)
    {
        const content = new contentModel(request.body);
        content.id = generateUUID();

        let pFile;
        // Dans le cas où l'on upload un fichier associé
        if(!!request.file) {
            content.fileName = `${content.id}.${mime.getExtension(request.file.mimetype)}`;
            content.setData(request.file);            
            pFile = _p(fs.readFile)(request.file.path);
        } else {
            pFile = Promise.resolve();
        }

        pFile.then((fileData) => {
            content.setData(fileData);

            contentModel.create(content, (err, data) => {
                if (err) return ContentController._error(response, 500, err);
                response.json(data); // TODO : comprendre pourquoi il ne retourne pas du JSON
            })
        }).catch((err) => this._error(response, 500, err));
    }

    static read (request, response)
    {
        const metaOnly = !!request.query.json;

        const id = request.contentId;
        contentModel.read(id, (err, content) => {
            if (err) return ContentController._error(response, 500, err);

            if(metaOnly) {
                response.json(content);
                return
            }

            if(content.type === 'img') {
                response.sendFile(contentModel._getAbsolutePath(content.fileName), { root: __basedir });
            } else {
                response.location(content.src);
            }
        });
    }

    static update (request, response)
    {
        const content = new contentModel(request.body);

        if(request.contentId !== content.id) return ContentController._error(response, 500, "id error");

        contentModel.update(content, (err, data) => {
            if (err) ContentController._error(response, 500, err);
            response.json(data);
        });
    }

    static delete (request, response)
    {
        const id = request.contentId;

        contentModel.delete(id, (err, data) => {
            if (err) ContentController._error(response, 500, err);
            response.json(data);
        });
    }

    static _error(response, status, data) {
        response.send(data)
                .status(status)
                .end();
    }
}

module.exports = ContentController;
