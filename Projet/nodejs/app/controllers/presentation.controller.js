'use strict';

const fs = require('fs');
const {promisify} = require('util');
const readFileAsync = promisify(fs.readFile);
const writeFileAsync = promisify(fs.writeFile);

const CONFIG = JSON.parse(process.env.CONFIG);

class PresentationController {

    static load(request, response) {
        readFileAsync(CONFIG.presentationFile).then((buffer) => {
            const content = buffer.toString();
            const data = JSON.parse(content.toString());

            response.send(data);
        }).catch((err) => {
            response.send(err.message);
            response.status(500).end();
        });
    }

    static save(request, response) {
        const data = request.body;
        if (!data.hasOwnProperty('id')) {
            response.status(400);
            response.send('id property wasn\'t found');
        }

        writeFileAsync(CONFIG.presentationFile, JSON.stringify(data)).then(() => {
            response.sendStatus(204);
        }).catch((err) => {
            response.status(400);
            response.send(err.message);
        });
    }
}

module.exports = PresentationController;
