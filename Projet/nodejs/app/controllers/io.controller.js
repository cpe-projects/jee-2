'use strict';

const clientMap = {};
let isStarted = false;

module.exports.listen = (httpServer) => {
    const io = require('socket.io')(httpServer);
    io.sockets.on('connection', (socket) => {

        socket.emit('connection');

        socket.on('_data_comm_', (sId) => {
            clientMap[sId] = socket;

            console.log("status", isStarted);
            if(isStarted) {
                socket.emit('slidEvent', { CMD: 'START', ID: 1});
            }
        });

        socket.on('slidEvent', (event) => {
            console.log('slidEvent', event);
            switch (event.CMD) {
                case 'START':
                    isStarted = true;
                    socket.broadcast.emit('slidEvent', { CMD: 'START', ID:1 })   
                    break;
                case 'PAUSE':
                   break;
                case 'BEGIN':
                    break;
                case 'END':
                    isStarted = false;
                    socket.broadcast.emit('slidEvent', { CMD: 'END' })   
                    break;
                case 'PREV':
                    socket.broadcast.emit('slidEvent', { CMD: 'PREV' })   
                    break;
                case 'NEXT':
                    socket.broadcast.emit('slidEvent', { CMD: 'NEXT' })   
                    break;
                default:
                    break;
            }
        });
    });
};
