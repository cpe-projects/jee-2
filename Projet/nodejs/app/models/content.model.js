'use strict';

const fs = require('fs');
const _p = require('util').promisify;

const CONFIG = JSON.parse(process.env.CONFIG);

class ContentModel {

    constructor({id, title, type, src, fileName} = {}) {
        this.id = id;
        this.title = title;

        this.type = type;

        this.src = src;
        this.fileName = fileName;

        let data;
        this.getData = () => {
            return data;
        };
        this.setData = (d) => {
            data = d;
        }
    }

    asJson() {
        return JSON.stringify(this);
    }

    /**
     * Prend un objet ContentModel en paramètre, stocke le contenu de [content.data] dans le fichier [content.fileName]
     * (dans le cas d'un contenu de type 'img') et stocke les meta-données
     * dans un fichier [contentModel.id].meta.json dans le répertoire [CONFIG.contentDirectory].
     *
     * Les meta-données sont obtenus en faisant un JSON.parse(content)
     *
     * @param {ContentModel} content
     * @param callback
     */
    static create(content, callback) {

        if (!content || !content.id || !(content instanceof ContentModel)) return callback(new Error("Content must not be null"));

        const metaPath = ContentModel._getAbsolutePath(`${content.id}.meta.json`);

        let p2;
        const data = content.getData();
        if (content.type === 'img' && data.length > 0) {
            const filePath = ContentModel._getAbsolutePath(content.fileName);
            content.src = filePath;
            
            p2 = _p(fs.writeFile)(filePath, data);
        } else {
            p2 = Promise.resolve();
        }

        p2.then(() => {
            _p(fs.writeFile)(metaPath, content.asJson()).then(() => callback(null, content.asJson())).catch(callback)
        }).catch(callback);

    }

    /**
     * Prend un id en paramètre et retourne l'objet ContentModel lu depuis le fichier [id].meta.json

     * @param id
     * @param callback
     */
    static read(id, callback) {

        if (!id) return callback(new Error('Aucun id passé en paramètre'));

        const path = ContentModel._getAbsolutePath(`${id}.meta.json`);

        fs.readFile(path, (err, fileContent) => {

            if (err) return callback(err);

            const data = JSON.parse(fileContent);
            let model = new ContentModel(data);
            model.setData(data.data);

            callback(null, model);
        });

    }

    static list(callback) {
        const dir = CONFIG.contentDirectory;

        const promises = [];
        fs.readdir(dir, (err, files) => {
            if(err) return callback(err);

            files.forEach((f) => {
                if(f.endsWith('.meta.json')) {
                    const id = f.replace('.meta.json', '');
                    promises.push(_p(ContentModel.read)(id));   
                }
            });

            Promise.all(promises).then((data) => callback(null, data))
                                 .catch((err) => callback(err));
        })
        
    }

    /**
     * Prend un objet ContentModel en paramètre et met à jour le fichier de metadata ([content.id].meta.json).
     * Le fichier [content.fileName] est mis à jour si [content.type] est égal à 'img' et
     * si [content.data] est renseigné (non nul avec une taille > 0).

     * @param {ContentModel} content
     * @param callback
     */
    static update(content, callback) {
        if (!content || !content.id) return callback(new Error('Aucun id passé en paramètre'));

        ContentModel.read(content.id, (err) => {
            if (err) return callback(err);
            ContentModel.create(content, callback);
        });
    }

    /**
     * Prend un id en paramètre et supprime les fichiers data ([content.fileName]) et metadata ([content.id].meta.json)

     * @param {int} id
     * @param callback
     */
    static delete(id, callback) {
        if (!id) return callback(new Error('Aucun id passé en paramètre'));

        console.log(`Suppresion des fichiers de ${id}`);

        ContentModel.read(id, (err, content) => {
            if (err) return callback(new Error('Fichier non existant'));

            // Il n'y a pas forcément de fichier binaire associé
            let pFile = (!!content.fileName) ? _p(fs.unlink)(ContentModel._getAbsolutePath(content.fileName)) : Promise.resolve();

            pFile.then(() => {
                _p(fs.unlink)(ContentModel._getAbsolutePath(`${content.id}.meta.json`)).then(() => {
                    callback(null);
                }).catch(callback);
            }).catch(callback);

        });
    }

    /**
     * Retourne le chemin d'un fichier du dossier content
     * @param {string} path
     * @returns {string}
     * @private
     */
    static _getAbsolutePath(path) {
        return `${CONFIG.contentDirectory}/${path}`;
    }

}

module.exports = ContentModel;
