'use strict';

const express = require('express');
const router = express.Router();
const multer = require("multer");

const contentController = require('../controllers/content.controller');
const multerMiddleware = multer({ "dest": "/tmp/" });

router.route('/contents').
    get(contentController.list).
    post(multerMiddleware.single("file"), contentController.create);

router.route('/contents/:contentId').
    get(contentController.read).
    put(contentController.update).
    delete(contentController.delete);

router.param('contentId', (req, res, next, id) => {
    req.contentId = id;
    next();
});

module.exports = router;