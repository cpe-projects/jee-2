'use strict';

const express = require('express');
const router = express.Router();
const presentationController = require('../controllers/presentation.controller');

router.route('/presentation').get(presentationController.load).put(presentationController.save);

module.exports = router;
