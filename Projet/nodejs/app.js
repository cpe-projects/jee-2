'use strict';

global.__basedir = __dirname;

const express = require('express');
const fs = require('fs');
const http = require('http');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const ioController = require('./app/controllers/io.controller');

const CONFIG = require('./config.json');
process.env.CONFIG = JSON.stringify(CONFIG);

const app = express();
app.use(bodyParser.json());
app.use(cors());

app.all('*', (req, res, next) => {
    const allowed = [
        /\/login/,
        /\/admin(?:\/?)/,
        /\/watch(?:\/?)/,
        /\/contents(?:\/?)/,
        /\/contents\/[a-z0-9\-]+(?:\/?)/,
        /\/presentation(?:\/?)/
    ];
    const isAllowed = allowed.find(r => req.originalUrl.match(r));

    if(isAllowed || !!req.query.role) { next(); return; }
    if(!req.headers.authorization) { res.status(403).end(); return; }

    const role = req.headers.authorization;
    if(req.method == 'GET' && !['user', 'admin'].includes(role)) { res.status(403).end(); return; }
    else if(role !== 'admin') { res.status(403).end(); return; }

    next();
});

const routePath = './app/routes/';
fs.readdirSync(routePath).forEach((file) => {
    if (path.extname(file) === '.js') {
        let route = require(routePath + file);
        app.use(route)
    }
});


app.use('/admin', express.static(path.join(__dirname, 'public/admin')));
app.use('/watch', express.static(path.join(__dirname, 'public/watch')));

const server = http.createServer(app);
server.listen(CONFIG.port);

ioController.listen(server);
