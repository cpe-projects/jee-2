export const setSelectedSlid = (slid_obj) => {
    return {
        type: 'UPDATE_SELECTED_SLID',
        obj: slid_obj
    };
};

export const updateContentMap = (contentMap) => {
    return {
        type: 'UPDATE_CONTENT_MAP',
        obj: contentMap
    };
};

export const updatePresentation = (presentation) => {
    return {
        type: 'UPDATE_PRESENTATION',
        obj: presentation
    };
};

export const updateSlid = (slid) => {
    return {
        type: 'UPDATE_PRESENTATION_SLID',
        obj: slid
    };
};

export const updateDraggedElt = (id) => {
    return {
        type: 'UPDATE_DRAGGED_ELT',
        obj: id
    };
};
