const updateModelReducer = (state = { presentation: { slidArray:[]}, contentMap: {} }, action) => {
    switch (action.type) {
        case 'UPDATE_PRESENTATION':
            return {
                contentMap: state.contentMap,
                presentation: action.obj,
            };
        case 'UPDATE_PRESENTATION_SLID':
            const idx = state.presentation.slidArray.findIndex(x => x.id === action.obj.id);
            state.presentation.slidArray[idx] = action.obj;

            return {
                contentMap: state.contentMap,
                presentation: JSON.parse(JSON.stringify(state.presentation))
            };
        case 'UPDATE_CONTENT_MAP':
            return { 
                contentMap: action.obj,
                presentation: state.presentation,
            };
        case 'ADD_CONTENT':
            return;
        default:
            return state;
    }
};

export default updateModelReducer;