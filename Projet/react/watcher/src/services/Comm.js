import io from 'socket.io-client';
import axios from 'axios';

export default class Comm {
    constructor() {
        this.comm = {};
        this.comm.io = {uuid: "...."};
        this.socket = io.connect('http://localhost:1337');
        this.emitOnConnect = this.emitOnConnect.bind(this);
        this.axios = axios.create({
            baseURL: 'http://localhost:1337',
        });

        if (localStorage.getItem('user')) {
            this.axios.defaults.headers.common['Authorization'] = JSON.parse(localStorage.getItem('user')).role;
        }
    }

    toString() {
        return '';
    }

    loadPres(callback, callbackErr) {
        this.axios.get(`/presentation/`)//${presId}`)
            .then(function (data) {
                callback(data.data);
            })
            .catch(function (error) {
                callbackErr(error);
            });

    }

    loadContent(callback, callbackErr) {
        this.axios.get('/contents')
            .then(function (data) {
                //console.log("raw content data");
                //console.log(data.data);
                var size = Object.keys(data.data).length;
                let contentMap = {};
                for (var i = 0; i < size; i++) {
                    let c_obj = data.data[Object.keys(data.data)[i]];
                    contentMap[c_obj.id] = c_obj;
                    // console.log(c_obj);
                }

                callback(contentMap);
            })
            .catch(function (error) {
                callbackErr(error);
            });

    }

    savPres(presJson, callbackErr) {
        this.axios.post('/savePres', presJson)
            .then(function (response) {
                console.log(response);
            })
            .catch(function (error) {
                callbackErr(error);
            });
    }

    savContent(contentJson, callbackErr) {
        this.axios.post('/addContent', contentJson)
            .then(function (response) {
                console.log(response);
            })
            .catch(function (error) {
                callbackErr(error);
            });
    }

    fileUpload(fileC, callback, callbackErr) {
        var data = new FormData();
        data.append('file', fileC);
        this.axios.post('/file-upload', data)
            .then(function (response) {
                console.log(response);
                callback();
            })
            .catch(function (error) {
                callbackErr(error);
            });

    }

    emitOnConnect(message) {
        console.log("message");
        console.log("socket");
        console.log(this.socket);
        console.log("this.comm.io.uuid");
        console.log(this.comm.io.uuid);
        this.socket.emit('data_comm', {'id': this.comm.io.uuid}, function (test) {
            console.log(test);
        });
    }

    socketConnection(uuid) {
        this.socket = io.connect(process.env.SOCKET_URL);
        this.comm.io.uuid = uuid;
        this.socket.on('connection', message => {
            this.emitOnConnect(message)
        });
    }

    onSlidEvent(callback) {
        this.socket.on('slidEvent', function (data) {
            callback(data);
        });
    }

    backward() {
        this.socket.emit('slidEvent', {'CMD': "PREV"});
    }

    forward() {
        this.socket.emit('slidEvent', {'CMD': "NEXT"});
    }

    play(presUUID) {
        this.socket.emit('slidEvent', {'CMD': "START", 'PRES_ID': presUUID});
    }

    pause() {
        this.socket.emit('slidEvent', {'CMD': "PAUSE"});
    }

    begin() {
        this.socket.emit('slidEvent', {'CMD': "BEGIN"});
    }

    end() {
        this.socket.emit('slidEvent', {'CMD': "END"});
    }

    login(username, password) {
        return this.axios.post('/login', {
            username: username,
            password: password
        })
    }

}
