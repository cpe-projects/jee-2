import React, {Component} from 'react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

import './Main.css';

import globalReducer from '../../reducers'

import WatcherComponent from '../watcherComponent/WatcherComponent';
import Login from '../loginComponent/Login';

const store = createStore(globalReducer);

class Main extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isLogged: !!localStorage.getItem('user')
        };

    }

    render() {        
        if(!this.state.isLogged) {
            return (<Login />);
        }

        return (
            <Provider store={store}>
            <div className='container-fluid height-100'>
                <div className="row height-100">
                    <div className='col-12'>
                        <WatcherComponent/>
                    </div>
                </div>
            </div>
            </Provider>
        );
    }
}

export default Main;
