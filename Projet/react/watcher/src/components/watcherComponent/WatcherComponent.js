import React, {Component} from 'react';
import {connect} from 'react-redux';

import {updatePresentation, updateContentMap, updateSlid} from '../../actions';

import './WatcherComponent.css';
import Slid from '../common/slid/containers/Slid';

import Comm from '../../services/Comm';

const comm = new Comm();

const mapStateToProps = (state, ownProps) => {
    console.log(state);
    return {
        presentation: state.updateModelReducer.presentation
    }
};
class WatcherComponent extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isReady: false,
            isEnded: false,
            currentSlidIdx: 0
        };

        
        this.initSocket();
    }

    initSocket() {
        comm.emitOnConnect("hello");
        comm.onSlidEvent((cb) => {
            let curIdx = this.state.currentSlidIdx;

            switch(cb.CMD) {
                case 'START': 
                    curIdx = 0;
                    comm.loadContent((data) => {
                        this.props.dispatch(updateContentMap(data));
                        comm.loadPres((pres) => {
                            this.props.dispatch(updatePresentation(pres));
                            this.setState({ isReady: true, isEnded: false });
                        }, (err) => {
                            console.error(err);
                        });
                    }, (err) => { });
                    
                    break;

                case 'NEXT':
                    curIdx++;
                    break;
                case 'PREV':
                    curIdx--;
                    break;
                case 'END':
                    this.setState({isEnded: true});
                    break;

                default: break;
            }

            if(curIdx >= 0 && curIdx < this.props.presentation.slidArray.length) {
                console.log('Switching to slid #', curIdx);
            
                this.setState({ currentSlidIdx: curIdx });

                const slid = this.props.presentation.slidArray[curIdx];
                this.props.dispatch(updateSlid(slid));
            }
        });
    }

    render() {       
        if(!this.state.isReady) {
            return (
                <p>Waiting...</p>
            );
        }

        if(this.state.isEnded) {
            return (
                <p>This is the end ;)</p>       
            )
        }

        const slid = this.props.presentation.slidArray[this.state.currentSlidIdx]; 
        return (
            <Slid slid={slid} displayMod="WATCH" />
        );
    }
}

export default connect(mapStateToProps)(WatcherComponent);
