import React, {Component} from 'react';
import {connect} from 'react-redux';

import {setSelectedSlid, updateSlid} from '../../../../actions';

import './EditMetaSlid.css'

const mapStateToProps = (state, ownProps) => {
    return {
        selected_slid: state.selectedReducer.slid,
    }
};

class EditMetaSlid extends Component {

    handleChangeTitle = (e) => {
        this.props.selected_slid.title = e.target.value;
        this.props.dispatch(updateSlid(this.props.selected_slid));
        this.props.dispatch(setSelectedSlid(this.props.selected_slid));
    };

    handleChangeTxt = (e) => {
        this.props.selected_slid.txt = e.target.value;
        this.props.dispatch(updateSlid(this.props.selected_slid));
        this.props.dispatch(setSelectedSlid(this.props.selected_slid));
    };

    render() {
        return (
            <div className="form-group">
                <label htmlFor="currentSlideTitle">Title </label>
                <input
                    type="text"
                    className="form-control"
                    id="currentSlideTitle"
                    onChange={this.handleChangeTitle}
                    value={this.props.selected_slid.title}
                />
                <label htmlFor="currentSlideText">Text</label>
                <textarea
                    rows="5"
                    className="form-control"
                    id="currentSlideText"
                    onChange={this.handleChangeTxt}
                    value={this.props.selected_slid.txt}>
                </textarea>
            </div>
        );
    }
}

export default connect(mapStateToProps)(EditMetaSlid);