import React, {Component} from 'react';
import Content from '../../content/containers/Content';
import {connect} from 'react-redux';

import {updateSlid, setSelectedSlid} from '../../../../actions';
import EditMetaSlid from '../components/EditMetaSlid';

import './Slid.css';

const mapStateToProps = (state, ownProps) => {
    return {
        draggedContentId: state.selectedReducer.draggedContentId
    }
};

class Slid extends Component {

    onDrop = (event) => {
        event.preventDefault();
        this.props.slid.content_id = this.props.draggedContentId;
        this.props.dispatch(updateSlid(this.props.slid));
        this.props.dispatch(setSelectedSlid(this.props.slid));
    };

    /*
     * Fixes onDrop not fired
     * @see https://stackoverflow.com/questions/50230048/react-ondrop-is-not-firing
     */
    static onDragOver(event) {
        event.stopPropagation();
        event.preventDefault();
    }

    renderEditMeta() {
        const slid = this.props.slid;
        if (this.props.displayMode === 'FULL_MNG') {
            return (
                <EditMetaSlid title={slid.title} txt={slid.txt}/>
            )
        }
    }

    render() {
        const slid = this.props.slid;
        return (
            <div className="card mb-4 shadow-sm" onDragOver={Slid.onDragOver} onDrop={this.onDrop}>
                <div className="card-body">
                    <p className="card-text">{slid.title}</p>
                    <p className="card-text">{slid.txt}</p>
                    <div className="d-flex justify-content-between align-items-center">
                        <small className="text-muted">#{slid.id}</small>
                    </div>
                </div>

                <Content id={slid.content_id}/>

                {this.renderEditMeta()}

            </div>
        );
    }
}

export default connect(mapStateToProps)(Slid);
