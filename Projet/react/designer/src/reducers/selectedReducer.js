const selectedReducer = (state = { slid: {}, draggedContentId: undefined }, action) => {
    switch (action.type) {
        case 'UPDATE_SELECTED_SLID':
            return {
                slid: JSON.parse(JSON.stringify(action.obj)),
                draggedContentId: state.draggedContentId
            };
        case 'UPDATE_DRAGGED_ELT':
            return {
                slid: state.slid,
                draggedContentId: action.obj
            };
        default:
            return state;
    }
};

export default selectedReducer;