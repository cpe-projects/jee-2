import uuid from 'uuid';

const updateModelReducer = (state = {presentation: {slidArray: []}, contentMap: {}}, action) => {
    switch (action.type) {
        case 'UPDATE_PRESENTATION': {
            return {
                contentMap: state.contentMap,
                presentation: action.obj,
            };
        }
        case 'UPDATE_PRESENTATION_SLID': {
            const idx = state.presentation.slidArray.findIndex(x => x.id === action.obj.id);
            state.presentation.slidArray[idx] = action.obj;

            return {
                contentMap: state.contentMap,
                presentation: JSON.parse(JSON.stringify(state.presentation))
            };
        }
        case 'UPDATE_CONTENT_MAP': {
            return {
                contentMap: action.obj,
                presentation: state.presentation,
            };
        }
        case 'ADD_CONTENT': {
            const content = action.obj;
            state.contentMap[content.id] = content;

            return {
                contentMap: JSON.parse(JSON.stringify(state.contentMap)),
                presentation: state.presentation
            };
        }
        case 'ADD_SLID': {
            const slid = action.obj;
            slid.id = uuid();
            state.presentation.slidArray.push(slid);

            return {
                contentMap: state.contentMap,
                presentation: JSON.parse(JSON.stringify(state.presentation))
            };
        }
        case 'DELETE_SLID': {
            const idx = state.presentation.slidArray.findIndex(x => x.id === action.obj.id);
            state.presentation.slidArray.splice(idx, 1);

            return {
                contentMap: state.contentMap,
                presentation: JSON.parse(JSON.stringify(state.presentation))
            };
        }
        default:
            return state;
    }
};

export default updateModelReducer;