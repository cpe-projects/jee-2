import io from 'socket.io-client';
import axios from 'axios';

export default class Comm {
    constructor() {
        this.comm = {};
        this.comm.io = {};
        this.socket = io.connect('http://localhost:1337');
        this.emitOnConnect = this.emitOnConnect.bind(this);
        this.axios = axios.create({
            baseURL: 'http://localhost:1337',
        });

        if (localStorage.getItem('user')) {
            this.axios.defaults.headers.common['Authorization'] = JSON.parse(localStorage.getItem('user')).role;
        }
    }

    toString() {
        return '';
    }

    loadPresentation(callback, callbackErr) {
        this.axios.get(`/presentation`)
            .then(function (data) {
                callback(data.data);
            })
            .catch(function (error) {
                callbackErr(error);
            });

    }

    updatePresentation(pres, callbackErr) {
        this.axios.put(`/presentation`, pres)
            .catch(function (error) {
                callbackErr(error);
            });
    }

    loadContent(callback, callbackErr) {
        this.axios.get('/contents')
            .then(function (data) {
                let size = Object.keys(data.data).length;
                let contentMap = {};
                for (let i = 0; i < size; i++) {
                    let c_obj = data.data[Object.keys(data.data)[i]];
                    contentMap[c_obj.id] = c_obj;
                }

                callback(contentMap);
            })
            .catch(function (error) {
                callbackErr(error);
            });

    }

    savContent(content) {
        return this.axios.post('/contents', content);
    }

    fileUpload(fileC, callback, callbackErr) {
        let data = new FormData();
        data.append('file', fileC);
        this.axios.post('/file-upload', data)
            .then(function (response) {
                console.log(response);
                callback();
            })
            .catch(function (error) {
                callbackErr(error);
            });

    }

    emitOnConnect(message) {
        this.socket.emit('_data_comm_', {'id': this.comm.io.uuid}, function (test) {
            console.log(test);
        });
    }

    socketConnection(uuid) {
        this.socket = io.connect(process.env.SOCKET_URL);
        this.comm.io.uuid = uuid;
        this.socket.on('connection', message => {
            this.emitOnConnect(message)
        });

        this.socket.on('newPres', function (socket) {

        });
        this.socket.on('slidEvent', function (socket) {

        });
    }

    backward() {
        this.socket.emit('slidEvent', {'CMD': "PREV"});
    }

    forward() {
        this.socket.emit('slidEvent', {'CMD': "NEXT"});
    }

    play(presUUID) {
        this.socket.emit('slidEvent', {'CMD': "START", 'PRES_ID': presUUID});
    }

    pause() {
        this.socket.emit('slidEvent', {'CMD': "PAUSE"});
    }

    begin() {
        this.socket.emit('slidEvent', {'CMD': "START"});
    }

    end() {
        this.socket.emit('slidEvent', {'CMD': "END"});
    }

    login(username, password) {
        return this.axios.post('/login', {
            username: username,
            password: password
        })
    }

}
