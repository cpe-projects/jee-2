import React, {Component} from 'react';
import {connect } from 'react-redux';

import Comm from '../../../services/Comm';

const comm = new Comm();

const mapStateToProps = (state, ownProps) => {
    return {
        presentation: state.updateModelReducer.presentation
    }
};

class PresCommands extends Component {

    onStart = () => {
        comm.begin(); 
    }

    onPrev = () => {
        comm.backward();
    }

    onNext = () => {
        comm.forward();        
    }

    onEnd = () => {
        comm.end();        
    }

    render() {
        return (
            <div className="form-group">
                <button onClick={this.onStart}>START</button>
                <button onClick={this.onPrev}>PREV</button>
                <button onClick={this.onNext}>NEXT</button>
                <button onClick={this.onEnd}>END</button>
            </div>
        );
    }

}

export default connect(mapStateToProps)(PresCommands);
