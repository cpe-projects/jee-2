import React, {Component} from 'react';
import {connect } from 'react-redux';

import Slid from '../../common/slid/containers/Slid';
import PresCommands from '../components/PresCommands';

import './EditSlidPanel.css';

const mapStateToProps = (state, ownProps) => {
    return {
        selected_slid: state.selectedReducer.slid
    }
};

class EditSlidPanel extends Component {

    render() {
        return (
            <div>
                <div className="card-body">
                    <PresCommands/>
                    <Slid slid={this.props.selected_slid} displayMode="FULL_MNG" />
                </div>
            </div>
        );
    }

}

export default connect(mapStateToProps)(EditSlidPanel);
