import React, {Component} from 'react';
import {connect} from 'react-redux';

import Content from '../../common/content/containers/Content';

import './BrowseContentPanel.css';
import AddContentPanel from "../../common/content/components/AddContentPanel";

const mapStateToProps = (state, ownProps) => {
    return {
        contentMap: state.updateModelReducer.contentMap
    }
};

class BrowseContentPanel extends Component {

    renderContents() {
        let contentMap = this.props.contentMap;

        return Object.keys(contentMap).map(k => (
            <Content key={k} id={contentMap[k].id} />
        ))
    }

    render() {
        let content = this.renderContents();

        return (
            <div className='browsecontentpanel'>
                <AddContentPanel/>
                {content}
            </div>
        );
    }

}

export default connect(mapStateToProps)(BrowseContentPanel);
