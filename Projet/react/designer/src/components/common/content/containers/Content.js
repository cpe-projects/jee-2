import React, {Component} from 'react';
import {connect} from 'react-redux';

import './Content.css';

import {updateDraggedElt} from '../../../../actions'
import ImageComponent from "../components/ImageComponent";
import VideoComponent from "../components/VideoComponent";
import WebComponent from "../components/WebComponent";

const mapStateToProps = (state, ownProps) => {
    return {
        contentMap: state.updateModelReducer.contentMap
    }
};

class Content extends Component {

    onDragStart = (event) => {
        const content = this.props.contentMap[this.props.id];
        this.props.dispatch(updateDraggedElt(content.id));

        // Fixes draggable on Firefox
        // @see https://stackoverflow.com/questions/3977596/how-to-make-divs-in-html5-draggable-for-firefox
        event.dataTransfer.setData('contentId', content.id);
    };

    renderMedia() {
        const content = this.props.contentMap[this.props.id];
        const type = content.type;
        const src = content.src;
        const title = content.title;

        switch (type) {
            case 'img_url':
                return (<ImageComponent src={src} title={title}/>);
            case 'img':
                return (<ImageComponent src={`http://localhost:1337${src}?role=user`} title={title}/>);
            case 'video':
                return (<VideoComponent src={src} title={title}/>);
            case 'web':
                return (<WebComponent src={src} title={title}/>);
            default:
                return (<div/>)
        }
    }

    render() {
        const content = this.props.contentMap[this.props.id];
        let media = this.renderMedia();
        return (
            <div className="card mb-4 shadow-sm" draggable="true" onDragStart={this.onDragStart}>
                {media}
                <div className="card-body">
                    <p className="card-text">{content.title}</p>
                    <small className="text-muted">#{content.id}</small>
                </div>
            </div>
        );
    }
}

export default connect(mapStateToProps)(Content);
