import React, {Component} from 'react';
import {connect} from 'react-redux';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import MenuItem from "@material-ui/core/MenuItem/MenuItem";
import {addContent} from '../../../../actions';
import Comm from '../../../../services/Comm';

const comm = new Comm();

class AddContentPanel extends Component {

    constructor(props) {
        super(props);
        this.state = {
            open: false,
            title: '',
            type: 'img_url',
            src: ''
        };
    }

    handleChangeTitle = (e) => {
        this.setState({title: e.target.value});
    };

    handleChangeSrc = (e) => {
        this.setState({src: e.target.value});
    };

    handleChangeType = (e) => {
        this.setState({type: e.target.value});
    };

    handleOpen = () => {
        this.setState({open: true});
    };

    handleClose = () => {
        this.setState({open: false});
    };

    handleSubmit = () => {
        const content = {
            title: this.state.title,
            type: this.state.type,
            src: this.state.src
        };

        comm.savContent(content).then((data) => {
            this.props.dispatch(addContent(JSON.parse(data.data)));
        }).catch((err) => {
            console.error(err);
        }).finally(() => {
            this.setState({
                title: '',
                type: 'img_url',
                src: ''
            });
            this.handleClose();
        });
    };

    render() {
        return (
            <div>
                <Button onClick={this.handleOpen}>Add new content</Button>
                <Dialog onClose={this.handleClose} open={this.state.open}>
                    <DialogTitle>Add new content</DialogTitle>
                    <TextField
                        label="Title"
                        value={this.state.title}
                        onChange={this.handleChangeTitle}
                        margin="normal"
                    />
                    <Select
                        value={this.state.type}
                        onChange={this.handleChangeType}
                    >
                        <MenuItem value={'img'}>Image</MenuItem>
                        <MenuItem value={'img_url'}>Image URL</MenuItem>
                        <MenuItem value={'video'}>Video</MenuItem>
                        <MenuItem value={'web'}>Web</MenuItem>
                    </Select>
                    <TextField
                        label="URL"
                        value={this.state.src}
                        onChange={this.handleChangeSrc}
                        margin="normal"
                    />
                    <Button onClick={this.handleSubmit}>Submit</Button>
                </Dialog>
            </div>
        );
    }

}

export default connect()(AddContentPanel);
