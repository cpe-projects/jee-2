import React, {Component} from 'react';

class WebComponent extends Component {
    
    render() {
        return (
            <iframe title={this.props.title} src={this.props.src}/>
        );
    }
}

export default WebComponent;
