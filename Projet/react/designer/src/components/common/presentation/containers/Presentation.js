import React, {Component} from 'react';

import EditMetaPres from '../components/EditMetaPres';
import SlidList from '../components/SlidList';
import CommandButtons from "../../../browsePresentationPanel/containers/CommandButtons";

class Presentation extends Component {

    render() {
        return (
            <div>
                <EditMetaPres/>
                <CommandButtons/>
                <SlidList/>
            </div>
        );
    }

}

export default Presentation;
