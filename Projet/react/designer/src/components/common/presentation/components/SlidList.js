import React, {Component} from 'react';
import {connect} from 'react-redux';

import {setSelectedSlid} from '../../../../actions'
import Slid from '../../slid/containers/Slid';

const mapStateToProps = (state, ownProps) => {
    return {
        slidArray: state.updateModelReducer.presentation.slidArray
    }
};

class SlidList extends Component {

    updateSelectedSlid = (slid) => {
        const tmpSlid = {
            id:slid.id,
            title:slid.title,
            txt:slid.txt,
            content_id:slid.content_id
        };

        this.props.dispatch(setSelectedSlid(tmpSlid));
    };

    render() {
        return this.props.slidArray.map((s) => (
            <div key={s.id} onClick={() => this.updateSelectedSlid(s)}>
                <Slid key={s.id} slid={s} displayMode="SHORT"/>
            </div>
        ))
    };
}

export default connect(mapStateToProps)(SlidList);
