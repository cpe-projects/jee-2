import React, {Component} from 'react';
import {connect} from 'react-redux';

import {updatePresentation} from '../../../../actions'

const mapStateToProps = (state, ownProps) => {
    return {
        pres: state.updateModelReducer.presentation
    }
};

class EditMetaPres extends Component {

    handleChangeTitle = (e) => {
        const tmpPres = {
            id: this.props.pres.id,
            title: e.target.value,
            description: this.props.pres.description,
            slidArray: this.props.pres.slidArray
        };

        this.props.dispatch(updatePresentation(tmpPres));
    };

    handleChangeDescription = (e) => {
        const tmpPres = {
            id: this.props.pres.id,
            title: this.props.pres.title,
            description: e.target.value,
            slidArray: this.props.pres.slidArray
        };

        this.props.dispatch(updatePresentation(tmpPres));
    };

    render() {

        return (
            <div className="form-group">
                <label htmlFor="curPresTitle">Title</label>
                <input
                    type="text"
                    className="form-control"
                    id="curPresTitle"
                    value={this.props.pres.title}
                    onChange={this.handleChangeTitle}
                />
                <label htmlFor="curPresDesc">Description</label>
                <textarea
                    rows="5"
                    type="text"
                    className="form-control"
                    id="curPresDesc"
                    value={this.props.pres.description}
                    onChange={this.handleChangeDescription}
                >
                </textarea>
            </div>
        );
    }

}

export default connect(mapStateToProps)(EditMetaPres);
