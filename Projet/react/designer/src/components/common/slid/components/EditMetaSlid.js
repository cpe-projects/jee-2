import React, {Component} from 'react';
import {connect} from 'react-redux';

import {setSelectedSlid, updateSlid, deleteSlid} from '../../../../actions';

import './EditMetaSlid.css'
import Button from "@material-ui/core/Button/Button";

const mapStateToProps = (state, ownProps) => {
    return {
        slidArray: state.updateModelReducer.presentation.slidArray,
        selected_slid: state.selectedReducer.slid,
    }
};

class EditMetaSlid extends Component {

    handleChangeTitle = (e) => {
        this.props.selected_slid.title = e.target.value;
        this.props.dispatch(updateSlid(this.props.selected_slid));
        this.props.dispatch(setSelectedSlid(this.props.selected_slid));
    };

    handleChangeTxt = (e) => {
        this.props.selected_slid.txt = e.target.value;
        this.props.dispatch(updateSlid(this.props.selected_slid));
        this.props.dispatch(setSelectedSlid(this.props.selected_slid));
    };

    handleDeleteSlid = () => {
        this.props.dispatch(deleteSlid(this.props.selected_slid));
        this.props.dispatch(setSelectedSlid(this.props.slidArray[0]));
    };

    renderRemoveButton() {
        if (this.props.slidArray.length > 1) {
            return (
                <div>
                    <Button onClick={this.handleDeleteSlid}>Remove slide</Button>
                    <hr/>
                </div>
            );
        }
    }

    render() {
        return (
            <div className="form-group">
                {this.renderRemoveButton()}
                <label htmlFor="currentSlideTitle">Title </label>
                <input
                    type="text"
                    className="form-control"
                    id="currentSlideTitle"
                    onChange={this.handleChangeTitle}
                    value={this.props.selected_slid.title}
                />
                <label htmlFor="currentSlideText">Text</label>
                <textarea
                    rows="5"
                    className="form-control"
                    id="currentSlideText"
                    onChange={this.handleChangeTxt}
                    value={this.props.selected_slid.txt}>
                </textarea>
            </div>
        );
    }
}

export default connect(mapStateToProps)(EditMetaSlid);