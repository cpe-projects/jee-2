import React, {Component} from 'react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

import './Main.css';

import BrowseContentPanel from '../browseContentPanel/components/BrowseContentPanel';
import Presentation from '../common/presentation/containers/Presentation';

import globalReducer from '../../reducers'
import EditSlidPanel from '../editSlidPanel/containers/EditSlidPanel';

import {setSelectedSlid, updateContentMap, updatePresentation} from '../../actions';
import Comm from '../../services/Comm';
import Login from '../loginComponent/Login';

const store = createStore(globalReducer);
const comm = new Comm();

class Main extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isReady: false,
            isLogged: false
        };

        comm.loadContent((data) => {
            store.dispatch(updateContentMap(data));

            comm.loadPresentation((pres) => {
                store.dispatch(setSelectedSlid(pres.slidArray[0]));
                store.dispatch(updatePresentation(pres));
                this.setState({isReady: true, isLogged: true});
            }, (err) => {
                console.error(err);
            });
        }, (err) => {
            console.error(err);
        });

        comm.emitOnConnect("salut");

    }

    render() {        
        if(!this.state.isReady) {
            return (<div><p>Loading...</p></div>)
        }

        if(!this.state.isLogged) {
            return (<Login />);
        }

        return (
            <Provider store={store}>
            <div className='container-fluid height-100'>
                <div className="row height-100">
                    <div className='col-md-3 col-lg-3 height-100 vertical-scroll'>
                        <Presentation/>
                    </div>
                    <div className='col-md-6 col-lg-6 height-100'>
                        <EditSlidPanel/>
                    </div>
                    <div className='col-md-3 col-lg-3 height-100'>
                        <BrowseContentPanel/>
                    </div>
                </div>
            </div>
            </Provider>
        );
    }
}

export default Main;
