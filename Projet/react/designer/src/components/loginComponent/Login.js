import React, {Component} from 'react';

import Comm from '../../services/Comm';

const comm = new Comm();

class Login extends Component {

    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password: '',
            error: false
        }
    }

    setUsername(username) {
        this.setState({ username: username });
    }

    setPassword(password) {
        this.setState({ password: password });
    }

    login() {
        comm.login(this.state.username, this.state.password).then((data) => {
            const user = data.data;
            if(!['admin'].includes(user.role)) {
                this.setState({error:true});
            } else {
                localStorage.setItem('user', JSON.stringify(user));
                window.location.reload();
            }
        }).catch(() => this.setState({error:true}));
    }

    getClassName() {
        return this.state.error ? 'error' : '';
    }

    render() {        
        return (
            <div className={this.getClassName() + 'container-fluid height-100'}>
                <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div className="form-group">
                        <label>Username</label>
                        <input type="text" className="form-control" value={this.state.username} onChange={(e) => this.setUsername(e.target.value)} placeholder="Enter username" />
                    </div>
                    <div className="form-group">
                        <label>Password</label>
                        <input type="password" className="form-control" value={this.state.password} onChange={(e) => this.setPassword(e.target.value)} placeholder="Password" />
                    </div>

                    <div className="form-group">
                        <button onClick={() => this.login()}>Login</button>
                    </div>

                </div>
            </div>
        );
    }
}

export default Login;
