import React, {Component} from 'react';
import {connect} from 'react-redux';
import Button from '@material-ui/core/Button';
import Comm from '../../../services/Comm';
import {addSlid} from "../../../actions";

const comm = new Comm();

const mapStateToProps = (state, ownProps) => {
    return {
        presentation: state.updateModelReducer.presentation
    }
};
class CommandButtons extends Component {

    handleAddSlide = () => {
        const slide = {
            title: '',
            txt: '',
            content_id: undefined
        };

        this.props.dispatch(addSlid(slide));
    };

    handleSavePresentation = () => {
        comm.updatePresentation(this.props.presentation);
    };

    render() {
        return (
            <div>
                <Button onClick={this.handleSavePresentation}>Save presentation</Button>
                <hr/>
                <Button onClick={this.handleAddSlide}>Add slide</Button>
            </div>
        );
    }

}

export default connect(mapStateToProps)(CommandButtons);
