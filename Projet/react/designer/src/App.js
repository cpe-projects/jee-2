import React, { Component } from 'react';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import './App.css';

import Main from './components/mainPanel/Main';

class App extends Component {
  render() {
    return (
      <div>
        <Main></Main>
      </div>
    );
  }
}

export default App;
