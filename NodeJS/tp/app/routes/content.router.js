'use strict';

const express = require('express');
const router = express.Router();
const multer = require("multer");

const contentController = require('../controllers/content.controller');
const multerMiddleware = multer({ "dest": "/tmp/" });

router.route('/contents').
    get(contentController.list).
    post(contentController.create);

router.route('/contents/:contentId').
    get(contentController.read).
    put(contentController.update).
    delete(contentController.delete);

router.params('contentId', (req, res, next, id) => {
    req.contentId = id;
    next();
});

router.post("/contents", multerMiddleware.single("file"), contentController.create);
