'use strict'

const express = require('express')
const router = express.Router()

router.route('/').
  get((request, response) => {
    response.send('It works !');
  });

module.exports = router
