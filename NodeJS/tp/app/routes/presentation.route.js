'use strict';

const fs = require('fs');
const path = require('path');
const express = require('express');
const router = express.Router();

const {promisify} = require('util');
const readFileAsync = promisify(fs.readFile);
const readdirAsync = promisify(fs.readdir);
const accessAsync = promisify(fs.access);
const writeFileAsync = promisify(fs.writeFile);

const CONFIG = JSON.parse(process.env.CONFIG);

router.route('/presentations').get((request, response) => {
    loadPresentations().then((result) => {
        response.send(result);
    }).catch((err) => {
        response.send(err.message);
        response.status(500).end();
    });
}).post((request, response) => {
    savePresentations(request.body).then(() => {
        response.sendStatus(201);
    }).catch((err) => {
        response.status(400);
        response.send(err.message);
    });
});

const loadPresentations = () => {
    return new Promise((resolve, reject) => {
        readdirAsync(CONFIG.presentationDirectory).then((files) => {
            const result = {};
            let wrongFiles = 0;
            for (let file of files) {
                if (path.extname(file) !== '.json') {
                    wrongFiles++;
                    continue;
                }

                readFileAsync(`${CONFIG.presentationDirectory}/${file}`).
                    then((buffer) => {
                        const content = buffer.toString();
                        const data = JSON.parse(content.toString());
                        result[data.id] = data;

                        if (files.length - wrongFiles ===
                            Object.keys(result).length) {
                            resolve(result);
                        }
                    }).
                    catch((err) => {
                        if (err) {
                            reject(err);
                        }
                    });
            }
        }).catch((err) => {
            if (err) {
                reject(err);
            }
        });
    });
};

const savePresentations = (data) => {
    return new Promise((resolve, reject) => {
        if (!data.hasOwnProperty('id')) {
            reject(new Error('id property wasn\'t found'));
        }

        const file = `${CONFIG.presentationDirectory}/${data.id}.pres.json`;
        accessAsync(file).then(() => {
            reject(new Error('File already exists'));
        }).catch((err) => {
            writeFileAsync(file, JSON.stringify(data)).then(() => {
                resolve(null);
            }).catch((err) => {
                reject(err);
            });
        });
    });
};

module.exports = router;
