const contentModel = require('../models/content.model');

class ContentController
{
    constructor () {}

    list ()
    {

    }

    create ()
    {

    }

    read (request, response)
    {
        const id = request.contentId;
        contentModel.read(id, (err, content) => {
            if (err) reject(err);
            resolve(content);
        });
    }

    update ()
    {

    }

    delete ()
    {

    }
}

export default ContentController;
