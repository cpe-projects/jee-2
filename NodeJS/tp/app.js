'use strict';

const express = require('express');
const fs = require('fs');
const http = require('http');
const path = require('path');
const bodyParser = require('body-parser');

const CONFIG = require('./config.json');
process.env.CONFIG = JSON.stringify(CONFIG);

const app = express();
app.use(bodyParser.json());
const routePath = './app/routes/';
fs.readdirSync(routePath).forEach((file) => {
    if (path.extname(file) === '.js') {
        let route = require(routePath + file);
        app.use(route)
    }
});

app.use('/admin', express.static(path.join(__dirname, 'public/admin')));
app.use('/watch', express.static(path.join(__dirname, 'public/watch')));

const server = http.createServer(app);
server.listen(CONFIG.port);

