import React, { Component } from 'react';
import './App.css';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      title: this.props.title,
      count: 0
    };
  }

  handleChangeTitle = (e) => {
    this.setState({
      title: e.target.value
    });
  }

  handleHover = (e) => {
    this.setState({
      count: this.state.count + 1
    });
  }

  render() {
    return (
      <div className="App">
        <h1 onMouseEnter={this.handleHover}>TD1 ({this.state.count})</h1>
        <label htmlFor="titleInput">Title: </label>
        <input type="text" id="titleInput" onChange={this.handleChangeTitle} value={this.state.title} />
        <h3>Result: {this.state.title}</h3>
      </div>
    );
  }
}

export default App;
