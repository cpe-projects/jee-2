import React, { Component } from 'react'
import './lib/bootstrap-3.3.7-dist/css/bootstrap.min.css'
import * as jsonSource from './sources/robots_parts.json'

import LeftSide from './components/LeftSide/LeftSide'
import MiddleSide from './components/MiddleSide/MiddleSide'

class Main extends Component {
  constructor (props) {
    super(props)
    let robotList

    robotList = jsonSource.default

    this.state = {
      robot_list: robotList,
      selected_robot_id: 0,
      selected_parts: []
    }
  }

  handleOnRobotSelected = (id) => {
    const current_robot_obj = this.getRobotFromId(id)
    this.setState({selected_robot_id: id})
    this.setState({selected_parts: current_robot_obj.parts})
  }

  getRobotFromId = (id) => {
    for (let i = 0; i < this.state.robot_list.robots.length; i++) {
      if (this.state.robot_list.robots[i].id === id) {
        return this.state.robot_list.robots[i]
      }
    }
    return {}
  }

  render () {
    return (
      <div className="container-fluid">
        <div className="row">
          <h1>Welcome to robot shop</h1>
        </div>
        <div className="row">
          <div className="col-md-4 col-lg-4">
            <LeftSide
              robots={this.state.robot_list.robots}
              handleOnRobotSelected={this.handleOnRobotSelected}
            />
          </div>
          <div className="col-md-4 col-lg-4">
            <MiddleSide
              parts={this.state.selected_parts}
              partlists={this.state.robot_list.parts}
            />
          </div>
          <div className="col-md-4 col-lg-4">

          </div>
        </div>
      </div>
    )
  }
}

//export the current classes in order to be used outside
export default Main
