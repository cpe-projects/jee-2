package fr.cpe.ejb.impl;

import fr.cpe.ejb.HelloService;

import javax.ejb.Stateless;

@Stateless
public class HelloServiceImpl implements HelloService {

    @Override
    public String sayHello() {
        return "Hello";
    }

}
