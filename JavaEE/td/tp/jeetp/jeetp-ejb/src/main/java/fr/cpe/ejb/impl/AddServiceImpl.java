package fr.cpe.ejb.impl;

import fr.cpe.ejb.AddService;

import javax.ejb.Local;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class AddServiceImpl implements AddService {

    @Override
    public Double add(List<Double> a) {
        return a.stream()
                .mapToDouble(Double::doubleValue)
                .sum();
    }
}
