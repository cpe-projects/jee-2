package fr.cpe.ejb.impl;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.ejb.Singleton;
import javax.ejb.Stateless;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import java.util.logging.Logger;

@MessageDriven(name = "HelloWorldMDB", activationConfig = {
// Queue ou Topic
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
// Nom JNDI
        @ActivationConfigProperty(propertyName = "destination", propertyValue = "jmstp"),
// Configuration pour reconnaitre automatiquement les messages recus
        @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge") })
@Singleton
public class HelloJmsConsumerImpl implements MessageListener {

    private static final Logger logger = Logger.getLogger(HelloJmsConsumerImpl.class.getName());

    @Override
    public void onMessage(Message var1) {
        logger.info(">>> onMessage() - " + var1.toString());
        try {
            if (var1 instanceof TextMessage) {
                String msg = ((TextMessage) var1).getText();
                logger.info(msg);
            }
        } catch (JMSException e) {
            e.printStackTrace();
        }
        logger.info("<<< onMessage()");
    }

}
