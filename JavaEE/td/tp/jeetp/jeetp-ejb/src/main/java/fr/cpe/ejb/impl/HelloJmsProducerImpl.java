package fr.cpe.ejb.impl;

import fr.cpe.ejb.HelloJmsProducer;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.Queue;

@Stateless
public class HelloJmsProducerImpl implements HelloJmsProducer {

    @Resource(name = "java:/jmstp")
    private Queue queue;

    //@JMSConnectionFactory(value = "java:comp/DefaultJMSConnectionFactory")
    @Inject
    private JMSContext context;

    @Override
    public void sendHello() {
        context.createProducer().send(queue, "Hello world !");
    }
}
