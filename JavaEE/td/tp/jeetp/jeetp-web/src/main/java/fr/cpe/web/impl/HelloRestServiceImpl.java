package fr.cpe.web.impl;

import fr.cpe.ejb.HelloJmsProducer;
import fr.cpe.ejb.HelloService;
import fr.cpe.web.HelloRestService;

import javax.inject.Inject;

public class HelloRestServiceImpl implements HelloRestService {

    @Inject
    HelloService helloService;

    @Inject
    HelloJmsProducer helloJmsProducer;

    @Override
    public String sayHello() {
        return helloService.sayHello();
    }

    @Override
    public void sayHelloAsync() {
        helloJmsProducer.sendHello();
    }
}
