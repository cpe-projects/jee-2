package fr.cpe.web.impl;

import fr.cpe.ejb.AddService;
import fr.cpe.ejb.HelloService;
import fr.cpe.web.AddRestService;
import fr.cpe.web.HelloRestService;

import javax.inject.Inject;
import java.util.List;

public class AddRestServiceImpl implements AddRestService {

    @Inject
    AddService addService;

    @Override
    public Double add(List<Double> a) {
        return addService.add(a);
    }
}
