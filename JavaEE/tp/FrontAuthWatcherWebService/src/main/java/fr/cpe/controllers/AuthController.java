package fr.cpe.controllers;

import fr.cpe.models.UserModel;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/auth")
public interface AuthController {

    @GET
    @Path("/health")
    String healthCheck();

    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    String login(String body);

}
