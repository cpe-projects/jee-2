package fr.cpe.controllers.impl;

import fr.cpe.controllers.AuthController;
import fr.cpe.factories.UserFactory;
import fr.cpe.models.UserModel;
import fr.cpe.jms.IMessageReceiver;
import fr.cpe.jms.IMessageSenderTopic;

import javax.inject.Inject;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.core.Response;
import java.util.logging.Logger;

public class AuthControllerImpl implements AuthController {

    private static Logger logger = Logger.getLogger(AuthControllerImpl.class.getCanonicalName());

    @Override
    public String healthCheck() {
        return "Alive";
    }

    @Inject
    UserFactory userFactory;

    @Inject
    IMessageSenderTopic messageSender;

    @Inject
    IMessageReceiver messageReceiver;

    @Override
    public String login(String body) {

        UserModel userModel = userFactory.createFromJson(body);

        userModel.setRole("ADMIN");
        userModel.setFirstname("Mathias-Laurent");
        userModel.setLastname("Morel-Arlaud");

        messageSender.sendMessage(userModel);

        logger.info("BONDOUR " + userModel.toJson(true));

        UserModel validUser = messageReceiver.receiveMessage();

        if(validUser == null) {
            throw new ForbiddenException();
        }

        return validUser.toJson(true);

    }

}
