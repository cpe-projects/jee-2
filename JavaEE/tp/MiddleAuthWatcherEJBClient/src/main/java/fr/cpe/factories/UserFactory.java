package fr.cpe.factories;

import fr.cpe.models.UserModel;

import javax.ejb.Singleton;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import java.io.StringReader;

@Singleton
public class UserFactory {

    public UserFactory() {
    }

    public UserModel createFromJson(String body) {
        JsonReader jsonReader = Json.createReader(new StringReader(body));
        JsonObject obj = jsonReader.readObject();

        UserModel userModel = new UserModel();

        if(obj.containsKey("id")) {
            userModel.setId(obj.getInt("id"));
        }

        if(obj.containsKey("login")) {
            userModel.setLogin(obj.getString("login"));
        }

        if(obj.containsKey("password")) {
            userModel.setPassword(obj.getString("password"));
        }

        if(obj.containsKey("firstname")) {
            userModel.setFirstname(obj.getString("firstname"));
        }
        if(obj.containsKey("lastname")) {
            userModel.setLastname(obj.getString("lastname"));
        }

        if(obj.containsKey("role")) {
            userModel.setRole(obj.getString("role"));
        }

        return userModel;
    }

}
