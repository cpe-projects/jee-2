package fr.cpe.jms;

import fr.cpe.models.UserModel;

public interface IMessageSenderTopic {

    void sendMessage(String message);
    void sendMessage(UserModel user);

}
