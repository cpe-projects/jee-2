package fr.cpe.dao;

import fr.cpe.models.UserModel;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Singleton
public class UserModelDao {

    @PersistenceContext
    EntityManager entityManager;

    public UserModel getByLoginAndPassword(String login, String password) {

        return (UserModel) entityManager.createNamedQuery("UserModel.byLoginAndPassword")
                .setParameter("login", login)
                .setParameter("password", password)
                .getSingleResult();

    }

}
