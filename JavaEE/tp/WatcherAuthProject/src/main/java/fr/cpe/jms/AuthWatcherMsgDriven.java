package fr.cpe.jms;

import fr.cpe.dao.UserModelDao;
import fr.cpe.models.UserModel;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.*;
import javax.persistence.NoResultException;
import java.util.Date;
import java.util.logging.Logger;

@MessageDriven(
        activationConfig = {
                @ActivationConfigProperty(
                        propertyName = "destinationType",
                        propertyValue = "javax.jms.Topic"),
                @ActivationConfigProperty(
                        propertyName = "destination",
                        propertyValue = "java:/jms/watcherAuthJMSx")
        })
public class AuthWatcherMsgDriven implements MessageListener {

    private static final Logger logger = Logger.getLogger(AuthWatcherMsgDriven.class.getCanonicalName());

    @Inject
    private UserModelDao userDao;

    @Inject
    IMessageSenderQueue sender;

    public void onMessage(Message message) {
        try {
            if (message instanceof TextMessage) {
                logger.info("Topic: I received a TextMessage at "
                                + new Date());
                TextMessage msg = (TextMessage) message;
                logger.info("Message is : " + msg.getText());
            } else if (message instanceof ObjectMessage) {
                logger.info("Topic: I received an ObjectMessage at " + new Date());
                ObjectMessage msg = (ObjectMessage) message;

                if( msg.getObject() instanceof UserModel){
                    UserModel user = (UserModel) msg.getObject();
                    logger.info("User Details: " + user.getLogin());
                    UserModel validUser = null;

                    try {
                        validUser = userDao.getByLoginAndPassword(user.getLogin(), user.getPassword());
                    } catch (NoResultException e) {
                        logger.info("User " + user.getLogin() + " not found :" + e.getMessage());
                    }

                    sender.sendMessage(validUser);
                }

            } else {
                logger.info("Not valid message for this Queue MDB");
            }
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}