package fr.cpe.jms.impl;

import fr.cpe.jms.IMessageSenderQueue;
import fr.cpe.models.UserModel;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.Queue;
import java.util.logging.Logger;

/**
 * Session Bean implementation class MessageSenderQueue
 */
@Stateless
public class MessageSenderQueue implements IMessageSenderQueue {
    private final static Logger logger = Logger.getLogger(MessageSenderQueue.class.getCanonicalName());

    @Inject
    JMSContext context;

    @Resource(mappedName = "java:/jms/queue/watcherqueue")
    Queue queue;

    public void sendMessage(String message) {
        logger.info("Message text envoyé sur la queue watcherqueue");
        context.createProducer().send(queue, message);
    }

    public void sendMessage(UserModel user) {
        logger.info("Message UserModel envoyé sur la queue watcherqueue");
        context.createProducer().send(queue, user);
    }
}