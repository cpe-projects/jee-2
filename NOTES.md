# NodeJS

- CLI, framework JS server, multi OS, open source
- écrit en C++ et JS
- pour appli réseaux supportant montées charge
- single-thread
- modèle E/S non-bloquant => events

## Modules
Basé sur des modules (os, sys, fs, etc.)

Module : `module.exports = () => {}`
main.js : `const mod = require('module')`

## Outils
- npm : gestionnaire de deps
- express : framework pour routing
- socket.io : lib websockets
  > serveur: myIO = io(server); myIO.on((socket) => socket.on(), socket.emit())
  > client : socket = io(); socket.emit(); socket.on();

# React
Framework front-end -> gain de temps, aide à organiser code, composants réutilisables

## Origine 
Pb de FB avec MVC : trop de dépendences transitives, gros merdier si on scale
-> solution : angular.js data binding (MVVM) = model màj vue et vue màj model
=> Angular plus simple à écrire mais plus lourd/ctrl données moins fin

=> FB lance Flux (Action -> Dispatcher -> Store -> View)
=> ensuite React.js

## Fonctionnement
- One-way binding
- Basé sur des .jsx (ES6 sauce React) -> transpilés en JS avec Babel
- DOM virtuel = light copy du DOM -> quicker updates
=> lors d'une modif d'un noeud du virtual DOM, le noeud (et ses enfants) du DOM sont re-render

- ReactComponent
- ReactNode
- ReactElement

## Component

props    -> ---------------------------------- 
            |				     |	
ui event -> |onXXXX() -> [state]  \	     | -> virtual DOM
	    |			  > render() | 
            |--------------------------------|

props : valeurs passées d'un composant parent à un enfant -> le parent gère le cycle de vie de la donnée
state : valeurs représentant l'état actuel du composant   -> le composant gère lui même ...

### Code
```
class MyComponent extends Component {

  constructor(props) { super(props);  ... }
  
  handleOnXX(e) { ... }

  render() {
    return (<div></div>);
  }

}

export default MyComponent;
```

`this.updateSelectedSlid=this.updateSelectedSlid.bind(this);`
Permet de binder `this` avec la méthode `updateSelectedSlid`. On peut utiliser une fat arrow à la place, pour ne pas avoir à binder `this`.

## Redux

Action -> Reducer -> Store -> View

Définitions:
1. Définir actions
2. Définir reducers
3. Créer le store (dans le main, passés aux élements via `<Provider store={store}>`

Utilisations
1. Le composant se connecte au store : `connect(mapStateToPros)(MyComponent)`
 
A quoi sert la ligne connect()(Slid) ? Pourquoi n’y a-t-il pas d’argument à connect() ?
-> permet de connecter `Slid` au store
-> l'argument de connect est la callback utilisée lorsque le composant souscrit aux actions

Que fait la ligne this.props.dispatch(setSelectedSlid(tmpSlid)) ?
-> dispatch l'action avec les args

A quoi correspond setSelectedSlid ?
-> l'action 

Que se passerait-il si un nouvel objet n’était pas créé ( tmpSlid ) ?
-> aucun impact sur la vue (car React recrée la vue)

# JavaEE
## JNDI : Java Naming and Directory Interface
Permet de connecter divers composants de manière abstraite
ex: récupérer les infos de connexion à une BDD `java:cpe/dev/BddTest`
ex: utilsié par les EJB pour trouver les EJB remote

## Déploiement
### EAR : top archive level
-> 1+ EJB (.jar), 1+ module web (.war), 1 descripteur de déploiement (`META-INF/application.xml`), un dossier lib/ pour les libs communes

### EJB JAR
-> 1+ EJB, 1 descripteur (META-INF/ejb-jar.xml), facultatif: META-INF/persistence.xml

### WAR
-> 1 composant web (servlet, JSP, JSF etc.), 1 desc. depl. (WEB-INF/web.xml):

### Avec Maven
		EAR
	/ contient modules  \
    EJB                    WAR
     |   (deps provided)   / \
 API JavaEE       API JavaEE EJB     

## Ressources (datasources, JMS, etc.)
Déclarées dans interface d'admin
Accès dans code via injection dép ou via JNDI

## EJB de Session
"Composant distribué **transactionnel**"
-> unité logicielle autonome traitant business
-> accessible à distance (via nom JNDI)

### Instanciation
1. Instancié et ajouté au pool d'EJB
2. Instanciation de ses dépendances si nécessaires
3. EJB rendu dispo (=> appel @PostContruct)

@Singleton : 1 seule et unique instance
@Stateless : 1 instance placée dans le pool d'EJB (et plusieurs lorsqu'il y a des accès concurrents)
@Stateful : 1 instance par connexion

Tip: 100 appels (10 concurrents) sur l'API avec Apache Benchmark 
ab -n 100 -c 10 http://localhost:8080/api/test

@Startup : instancié au démarrage de l'appli (utile pour la conf) 
@PostConstruct

## EJB Message Driven
Composant encapsulant la logique business, déclenché par envoi messages async

## JMS
Queue : 1 seul subscriber
Topic : n subscribers (pas de ack) 

### Producer
@Inject le `JmsContext`

### Consumer
Annoté @MessageDriven, et implémente MessageListener

## Persistence

### JPA
API pour : définir entités métier, CRUD, config ORM

- persistence unit = META-INF/persistence.xml
- entity : @Entity
- entity manager : EntityManager
- query/criteria : exécutées via EntityManager


## Autres : transactions, timers
